﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;
using RenderHeads.Media.AVProVideo;
using Vuforia;
public class MainBehaviour : MonoBehaviour {

    // Use this for initialization
    public Text header;
    public Text content;

    public EasyTween currentopen;

    public string[] header_;
    public string[] content_;

    public GameObject next;
    public GameObject previous;
    public GameObject panel;
    public GameObject start_button;

    public string scene;
    public Color loadToColor = Color.white;

    public int count = 0;

	void Start () {
        StartCoroutine(setauto());
        currentopen = panel.GetComponent<EasyTween>();
       
        currentopen.OpenCloseObjectAnimation();
       
    }
	
  public  void go_next()
    {

        StartCoroutine(nextit());
    }

    IEnumerator nextit()
    {
       
        Debug.Log("aaaa");
        if (count < 2)
        { 
            Debug.Log("aaaa222");
            currentopen = header.GetComponent<EasyTween>();
           
            
            count++;
            header.GetComponent<Text>().text = header_[count];

           // currentopen.OpenCloseObjectAnimation();
            yield return new WaitForSeconds(0.2f);

            currentopen = content.GetComponent<EasyTween>();


            content.GetComponent<Text>().text = content_[count];

          //  currentopen.OpenCloseObjectAnimation();
            Debug.Log(count + "counttt");
            if (count == 2)
            {
                start_button.SetActive(true);
            }

        }

    }

    public   void go_previous()
    {
        StartCoroutine(previt());
    }

    IEnumerator previt()
    { 

        if (count >0)
        {
            start_button.SetActive(false);
            Debug.Log("aaaa222");
            currentopen = header.GetComponent<EasyTween>();


            count--;
            header.GetComponent<Text>().text = header_[count];

            // currentopen.OpenCloseObjectAnimation();
            yield return new WaitForSeconds(0.2f);

            currentopen = content.GetComponent<EasyTween>();


            content.GetComponent<Text>().text = content_[count];

            //  currentopen.OpenCloseObjectAnimation();
            Debug.Log(count + "counttt");

        }
    }
	// Update is called once per frame
	void Update () {
		
	}

    IEnumerator setauto()
    {
        yield return new WaitForSeconds(0.02f);
        Screen.orientation = ScreenOrientation.LandscapeLeft;
        yield return new WaitForSeconds(0.8f);
        Screen.orientation = ScreenOrientation.AutoRotation;
        Screen.autorotateToPortrait = false;
        Screen.autorotateToLandscapeLeft = true;
        Screen.autorotateToLandscapeRight = true;



    }
    public void goo()

    {
        //  gameObject.GetComponent<Mask>().enabled = true;
        StartCoroutine(load());

        Debug.Log("xxx");
    }

    IEnumerator load()
    {
        PlayerPrefs.SetString("Tutorial", "true");
        // gameObject.GetComponent<Button>().enabled = false;
        //     iTween.FadeTo(gameObject, iTween.Hash("alpha", 0.4f, "time", 0.15f));
        //    yield return new WaitForSeconds(0.15f);
        //    iTween.FadeTo(gameObject, iTween.Hash("alpha", 1f, "time", 0.15f));
        yield return new WaitForSeconds(0.4f);
        //   Initiate.Fade("Main", loadToColor, 0.8f);
        Initiate.Fade(scene, loadToColor, 1f);
        //  Application.LoadLevel("Main");
    }

}
