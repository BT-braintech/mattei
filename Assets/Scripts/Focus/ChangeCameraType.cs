﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using Vuforia;
using UnityEngine.UI;

public class ChangeCameraType : MonoBehaviour {

	public Camera RayCamera;

	public Sprite CameraFront;
	public Sprite CameraReverse;

	public UnityEngine.UI.Image CameraTake; 

	private enum CameraModes {CAMERA_BACK, CAMERA_FRONT};
	private CameraModes CameraMode = CameraModes.CAMERA_BACK;
	private CameraDevice.CameraDirection cameraDirection;

	private CameraDevice.FocusMode mFocusMode = CameraDevice.FocusMode.FOCUS_MODE_NORMAL;
	private focusSceneObject focusInScene;

	void Awake()
	{
		focusInScene = GetComponent<focusSceneObject>();
	}

	public void ChangeCameraDirection()
	{
		bool result = CameraDevice.Instance.Stop();

		ApplyNewCamera( (CameraMode == CameraModes.CAMERA_FRONT) );

		result = CameraDevice.Instance.Init(cameraDirection);

		if (!result)
		{
			ChangeToFrontIfNoCameraPresented();
		}
		else
		{
			result = CameraDevice.Instance.Start();

			if (!result)
			{
				ChangeToFrontIfNoCameraPresented();
			}
		}
	}

	private void ApplyNewCamera(bool isCameraInFront)
	{
		cameraDirection = 
			(isCameraInFront) ? 
				CameraDevice.CameraDirection.CAMERA_BACK : 
				CameraDevice.CameraDirection.CAMERA_FRONT;

		CameraTake.sprite = (isCameraInFront) ? CameraFront : CameraReverse;
		
		CameraMode = (isCameraInFront) ? CameraMode = CameraModes.CAMERA_BACK : CameraMode = CameraModes.CAMERA_FRONT;
	}

	private void ChangeToFrontIfNoCameraPresented()
	{
		CameraDevice.CameraDirection cameraDirection = CameraDevice.CameraDirection.CAMERA_FRONT;

		Tracker tr = TrackerManager.Instance.GetTracker<Tracker>() as Tracker;
		tr.Stop();

		CameraDevice.Instance.Stop();
		CameraDevice.Instance.Deinit();

		StartCoroutine(WaitForCameraToChange());
	}

	IEnumerator WaitForCameraToChange()
	{
		yield return new WaitForSeconds(.5f);
		CameraDevice.Instance.Init(cameraDirection);
		CameraDevice.Instance.Start();

		Tracker tr = TrackerManager.Instance.GetTracker<Tracker>() as Tracker;
		tr.Start();
	}

	void Update()
	{
		if (Input.GetMouseButtonDown(0) && CameraMode == CameraModes.CAMERA_BACK)
		{
			if (!focusInScene.GetTouchObjectStatus() 
			    && !CheckIfGUITouch()
			    && !CheckIfTouchedPhysic())
			{
				FocusNow();
			}
		}
	}

	private bool CheckIfTouchedPhysic()
	{
		Ray ray = RayCamera.ScreenPointToRay (Input.mousePosition);
		RaycastHit hit;	

		return Physics.Raycast( ray, out hit) ? true : false;		
	}

	private bool CheckIfGUITouch()
	{
		PointerEventData pe = new PointerEventData(EventSystem.current);
		pe.position =  Input.mousePosition;
		
		List<RaycastResult> hits = new List<RaycastResult>();
		EventSystem.current.RaycastAll( pe, hits );

		return (hits.Count > 0) ? true : false;
	}
	
	private void FocusNow()
	{		
		focusInScene.FocusInScene();
		FocusModeActivated();
	}

	private void FocusModeActivated()
	{
		if (CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_TRIGGERAUTO)) {
			mFocusMode = CameraDevice.FocusMode.FOCUS_MODE_TRIGGERAUTO;   
		}
		else
		{
			CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_NORMAL);
			mFocusMode = CameraDevice.FocusMode.FOCUS_MODE_NORMAL;
		}
	}
}