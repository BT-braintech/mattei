﻿using UnityEngine;
using System.Collections;

public class focusSceneObject : MonoBehaviour {

	public GameObject Focus;
	public GameObject TouchObject;
	public GameObject DisableTouch;

	public void FocusInScene()
	{
		Focus.SetActive(false);
		Focus.SetActive(true);
		
		if (DisableTouch)
		{
			if (DisableTouch.activeInHierarchy)
			{
				DisableTouch.SetActive(false);
			}
		}

		StopAllCoroutines();
		StartCoroutine(SetFalseFocus());
	}

	IEnumerator SetFalseFocus()
	{
		yield return new WaitForSeconds(1.3f);
		Focus.SetActive(false);
	}

	public bool GetTouchObjectStatus()
	{
		if (TouchObject)
		{
			return TouchObject.activeInHierarchy;
		}

		return false;
	}
}
