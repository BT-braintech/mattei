/*==============================================================================
Copyright (c) 2010-2014 Qualcomm Connected Experiences, Inc.
All Rights Reserved.
Confidential and Proprietary - Protected under copyright and other laws.
==============================================================================*/

using UnityEngine;
using UnityEngine.UI;
namespace Vuforia
{
    /// <summary>
    /// A custom handler that implements the ITrackableEventHandler interface.
    /// </summary>
    public class DefaultTrackableEventHandler : MonoBehaviour,
                                                ITrackableEventHandler
    {
        #region PRIVATE_MEMBER_VARIABLES

        private TrackableBehaviour mTrackableBehaviour;
        public GameObject ga;
        public GameObject canvas;
        public Button pressing;
        public GameObject Fullscreen;
        public static bool checker = false;

        public static bool screenshot_check = false;

        public static bool video_playing = false;

        public GameObject download_button;

        public static bool check_tutorial = false;
        #endregion // PRIVATE_MEMBER_VARIABLES



        #region UNTIY_MONOBEHAVIOUR_METHODS

        void Start()
        {
            mTrackableBehaviour = GetComponent<TrackableBehaviour>();
            if (mTrackableBehaviour)
            {
                mTrackableBehaviour.RegisterTrackableEventHandler(this);
            }
        }

        #endregion // UNTIY_MONOBEHAVIOUR_METHODS



        #region PUBLIC_METHODS

        /// <summary>
        /// Implementation of the ITrackableEventHandler function called when the
        /// tracking state changes.
        /// </summary>
        public void OnTrackableStateChanged(
                                        TrackableBehaviour.Status previousStatus,
                                        TrackableBehaviour.Status newStatus)
        {
            if (newStatus == TrackableBehaviour.Status.DETECTED ||
                newStatus == TrackableBehaviour.Status.TRACKED ||
                newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
            {
                OnTrackingFound();
            }
            else
            {
                OnTrackingLost();
            }
        }

        #endregion // PUBLIC_METHODS

        
        public void redoo()
        {
            OnTrackingFound();
        }

        public void lose_ontutorial()
        {
            OnTrackingLost();
        }


        #region PRIVATE_METHODS

        public void getlost()
        {
            OnTrackingLost();
        }
        private void OnTrackingFound()
        {

            if (check_tutorial == false)
            {
                video_playing  = true;
                Move.checkingmark = true;
                Fullscreen.SetActive(true);
                Panelinfo.name_target = gameObject.transform.name;
                Fullscreen.GetComponentInChildren<Text>().text = gameObject.transform.name;
                Fullscreen.GetComponent<TextMesh>().text = gameObject.transform.name;
                Move.tracked = int.Parse(gameObject.transform.name);
                if (PlayerPrefs.GetString(gameObject.transform.name + "pref") == "yes")
                {
                    download_button.SetActive(true);
                    download_button.GetComponentInChildren<Text>().text = gameObject.transform.name;

                    Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
                    Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);

                    // Enable rendering:
                    foreach (Renderer component in rendererComponents)
                    {
                        component.enabled = true;
                    }

                    // Enable colliders:
                    foreach (Collider component in colliderComponents)
                    {
                        component.enabled = true;
                    }

                    Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " found");

                    int nr = int.Parse(gameObject.transform.name);
                    ga.transform.GetComponent<Move>().playit(nr);
                }
                else
                {

                    canvas.SetActive(true);
                    pressing.GetComponentInChildren<Text>().text = gameObject.transform.name;
                    ga.transform.GetComponent<Move>().corotinestarting0();


                }
            } 
        }


        private void OnTrackingLost()
        {
            Fullscreen.SetActive(false);
            Move.checkingmark = false;
            video_playing = false;
            ga.transform.GetComponent<Move>().stopit();
            Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
            Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);

            // Disable rendering:
            foreach (Renderer component in rendererComponents)
            {
                component.enabled = false;
            }

            // Disable colliders:
            foreach (Collider component in colliderComponents)
            {
                component.enabled = false;
            }

            Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " lost");

           if (PlayerPrefs.GetString(gameObject.transform.name + "pref") == "yes" && checker == true )
            {
                download_button.SetActive(false);
                if (Move.Expand_check == false && Panelinfo.tutorial_isopen == false && screenshot_check == false)
                {
                    Debug.Log("ktu false  expandi " + Move.Expand_check);
                    ga.transform.GetComponent<Move>().expand(Fullscreen);
                    
                }
                else if(Move.Expand_check == true)
                {
                    Debug.Log("ktu true " + Move.Expand_check);
                  Move.Expand_check = false;
                }
            }
            video_playing = false;

            #endregion // PRIVATE_METHODS
        }
    }
}
