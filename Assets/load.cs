﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class load : MonoBehaviour {

    public GameObject small;
    public GameObject big;
    public static ScreenOrientation orientation;
    // Use this for initialization

    public string scene;
    public Color loadToColor = Color.white;

    void Awake()
    {
        Screen.orientation = ScreenOrientation.Portrait;
        Screen.autorotateToLandscapeLeft = false;
        Screen.autorotateToLandscapeRight = false;
        Screen.autorotateToPortrait = true;
        
     //   Screen.orientation = ScreenOrientation.Portrait;

    }


    void Start () {
       // PlayerPrefs.DeleteKey("Tutorial");
        Screen.orientation = ScreenOrientation.Portrait;
        Screen.autorotateToLandscapeLeft = false;
        Screen.autorotateToLandscapeRight = false;
        Screen.autorotateToPortrait = true;
        
      //  Screen.orientation = ScreenOrientation.Portrait;
        // Screen.orientation = ScreenOrientation.Portrait;
    }
   

    
   public void press()
    {
        StartCoroutine(animate());
    }
	// Update is called once per frame
	void Update () {
		
	}

    IEnumerator animate()
    {
        iTween.ScaleAdd(small, new Vector3(0.3f,0.3f,0.3f), 0.2f);
        yield return new WaitForSeconds(0.18f);
        iTween.ScaleTo(small, Vector3.zero, 0.5f);
        yield return new WaitForSeconds(0.1f);
        iTween.ScaleAdd(big, new Vector3(0.3f, 0.3f, 0.3f), 0.2f);
        yield return new WaitForSeconds(0.18f);
        iTween.ScaleTo(big, Vector3.zero, 0.5f);
        yield return new WaitForSeconds(0.4f);
        RectTransform myrec = GetComponent<RectTransform>();
        myrec.localPosition += Vector3.right;
        iTween.MoveBy(gameObject, new Vector3(-1000f, 0f, 0f), 1.5f);
        yield return new WaitForSeconds(1f);
        Initiate.Fade(scene, loadToColor, 1f);
      

    }
}
