﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;
using UnityEngine.UI;

public class Panelinfo : MonoBehaviour {
    private EasyTween currentopen;
    public static bool tutorial_isopen = false;
    public GameObject tutor;
    public int counter = 0;
    public static string name_target;
    public GameObject button_download;
	// Use this for initialization
	void Start () {
  
	}

    public void animate_bar_open()
    {
        StartCoroutine(info_open());
    }
    public void animate_bar_close(GameObject info_button)
    {
        StartCoroutine(info_close(info_button));
    }

    public void open_tutorial(GameObject tutorial)
    {
        StartCoroutine(open_it(tutorial));

       
       
        
    }



    public void download_button(GameObject textcomp)
    {


        //    GameObject.Find(tracked.ToString()).transform.GetComponent<DefaultTrackableEventHandler>().redoo();

        StartCoroutine(redownload(textcomp));
    }
     
    IEnumerator redownload(GameObject textcomp)
    {
        PlayerPrefs.DeleteKey(textcomp.transform.GetComponent<Text>().text + "pref");

        GameObject.Find(textcomp.transform.GetComponent<Text>().text).transform.GetComponent<DefaultTrackableEventHandler>().getlost();
        yield return new WaitForSeconds(0.2f);
        button_download.SetActive(false);
        GameObject.Find(textcomp.transform.GetComponent<Text>().text).transform.GetComponent<DefaultTrackableEventHandler>().redoo();

    }

    public void quit()
    {
        StartCoroutine(quit_app());
    }

    public void make_it_true()
    {
        DefaultTrackableEventHandler.screenshot_check = true;
    }
    public void make_it_false()
    {
        DefaultTrackableEventHandler.screenshot_check = false;
    }

    IEnumerator quit_app()
    {
        yield return new  WaitForSeconds(0.32f);
        Application.Quit();
    }
    IEnumerator open_it(GameObject tutorial)
    {
        yield return new WaitForSeconds(0.1f);
        tutor = tutorial;
        //  tutorial_isopen = true;

        if (counter == 0)
        {    
           // Move mov = new Move();

         //   Debug.Log(mov.get_play());
         
            DefaultTrackableEventHandler.check_tutorial = true;

            
            tutorial_isopen = true;
            tutorial.SetActive(true);
          
            Debug.Log("ktu behet true dhe video playing esht " + DefaultTrackableEventHandler.video_playing);
            
            if (DefaultTrackableEventHandler.video_playing == true)
            {
                // DefaultTrackableEventHandler
                Debug.Log("emri ktij" + name_target);
                Move.Expand_check = false;
                GameObject.Find(name_target).transform.GetComponent<DefaultTrackableEventHandler>().lose_ontutorial();
            }
            currentopen = tutorial.GetComponent<EasyTween>();
            currentopen.OpenCloseObjectAnimation();
            counter++;
        }
        else if (counter > 0)
        {
            counter = 0;
            tutorial_isopen = false;
            DefaultTrackableEventHandler.check_tutorial = false;
            Debug.Log("ktu behet false");
            currentopen = tutorial.GetComponent<EasyTween>();
            currentopen.OpenCloseObjectAnimation();
            yield return new WaitForSeconds(0.6f);
            tutorial.SetActive(false);
        }
       
    }


    public void email_send(string email)
    {
        string t =
 "mailto:" + email;
        Application.OpenURL(t);
    }

    public void website(string web)
    {
        string t =
 web;
        Application.OpenURL(t);
    }

    
    IEnumerator info_open()
    {

      
        currentopen = gameObject.GetComponent<EasyTween>();
        currentopen.OpenCloseObjectAnimation();
        yield return new WaitForSeconds(0.2f);
      GameObject.Find("Info_expand").SetActive(false);
      //  StartCoroutine(info_open());
    }
    IEnumerator info_close(GameObject info)
    {

        yield return new WaitForSeconds(0.1f);
        currentopen = gameObject.GetComponent<EasyTween>();
        currentopen.OpenCloseObjectAnimation();
      if(tutorial_isopen== true)
        {
            tutorial_isopen = false;
            tutor.SetActive(true);
            DefaultTrackableEventHandler.check_tutorial = false;
            counter = 0;
            Debug.Log("ktu ca o " + tutorial_isopen);
            currentopen = tutor.GetComponent<EasyTween>();
            currentopen.OpenCloseObjectAnimation();
            yield return new WaitForSeconds(0.6f);
            tutor.SetActive(false);




        }
        
        yield return new WaitForSeconds(0.35f);
        info.SetActive(true);
        gameObject.SetActive(true);
        //  StartCoroutine(info_open());
    }

    // Update is called once per frame
    void Update () {
		
	}
}
