﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
#pragma warning disable 0168 // variable declared but not used.
#pragma warning disable 0219 // variable assigned but not used.
using System;
using System.IO;
using System.Runtime.InteropServices;
using FrispGames.Social.Api;
//using Vuforia;

public class TakePictureAndSave : MonoBehaviour {
	
	public Image[] DisableImages;
	public GameObject[] ObjectToDisableAndAcitvate;
	public GameObject[] ObjectToActivateAndDisable;
	public MonoBehaviour[] DisableScripts;
	
	public EasyTween ButtonsShare;
	
	public EasyTween FlashAnm;
	public EasyTween PopUp;
	public GameObject PhotoPreview;
	public GameObject RedDotFoto;
	
	private List<bool> ObjectState = new List<bool>();
	
	private bool pictureTaken = false;
	
	private SavePhotoInGallery savePhotoInGallery;
	
	private byte[] photoData;
	
	public static string LinkToGallery;
	
	public Texture2D FotoApp;
	
	Texture2D screenShot;

    public GameObject panelinfo;
	
	
	private const string TITLE   = "";
	private const string MESSAGE = "";

	public GameObject OVcamera;

	public GameObject LoadingShare;

//	public BepartCloudHandler CloudCtrl;
	
	private void Awake()
	{
		/*float ratio = (float) Screen.width / (float) Screen.height;
		if (PhotoPreview)
		PhotoPreview.GetComponent<RectTransform>().localScale = new Vector3(ratio, 1f, 1f);*/
		
		savePhotoInGallery = gameObject.AddComponent<SavePhotoInGallery>();
		
		CheckIfFotoSaved();
	}
	
	private void Start()
	{
		if (PlayerPrefs.GetInt("FotoApp") == 0)
		{
			//StartCoroutine(savePhotoInGallery.SavePhoto(FotoApp.EncodeToPNG()));
			PlayerPrefs.SetInt("FotoApp", 1);
		}
	}
	
	public void GetPicture()
	{
		if (!pictureTaken)
		{
            panelinfo.GetComponent<Panelinfo>().make_it_true();
           // DefaultTrackableEventHandler.screenshot_check = true;
			StartCoroutine(TakeScreen());
		}
	}
	
	private void CheckIfFotoSaved()
	{
		if (RedDotFoto)
		{
			if (System.IO.File.Exists(LinkToGallery))
			{
				
				RedDotFoto.SetActive(true);
			}
			else
			{
				RedDotFoto.SetActive(false);
			}
		}
	}
	
	void OnApplicationPause(bool pauseStatus)
	{
		if(LoadingShare != null)
			LoadingShare.SetActive(false);
		print("pauseStatus>>>" + pauseStatus);;
		if (!pauseStatus)
		{
			CheckIfFotoSaved();
		}
		else {
			LoadingShare.SetActive(false);
		}
	}
	
	public void OpenGalleryPhoto()
	{		
		Invoke("OpenPhoto", .6f);
	}
	
	private void OpenPhoto()
	{
		#if UNITY_ANDROID
		if (!string.IsNullOrEmpty( LinkToGallery ) && System.IO.File.Exists(LinkToGallery))
		{
			try
			{
				Application.OpenURL( LinkToGallery );
			}
			catch
			{
				CheckIfFotoSaved();
				
				if (PopUp && !PopUp.IsObjectOpened() ) PopUp.OpenCloseObjectAnimation();
			}
		}
		else
		{
			if (PopUp && !PopUp.IsObjectOpened()) PopUp.OpenCloseObjectAnimation();
		}
		#else
		Application.OpenURL("photos-redirect://");
		#endif
	}
	
	private IEnumerator TakeScreen()
	{
		DeactivateObjects();
		
		yield return new WaitForEndOfFrame();



		screenShot = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
		screenShot.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
		screenShot.Apply();

		if (FlashAnm)
		{
			FlashAnm.OpenCloseObjectAnimation();
		}

		yield return new WaitForSeconds(0.4f);
		
		PhotoPreview.SetActive(true);
		PhotoPreview.GetComponent<Image>().sprite = Sprite.Create(screenShot, new Rect(0, 0, screenShot.width, screenShot.height), Vector2.zero);

		pictureTaken = false;
		photoData = screenShot.EncodeToPNG();

		ButtonsShare.OpenCloseObjectAnimation();


		ActivateObjects();
		
		//StartCoroutine(GetScreenPhotoBytes());
		
		/*if (PhotoPreview)
		{
			if (!PhotoPreview.activeSelf)
			{
				PhotoPreview.SetActive(true);
			}

			PhotoPreview.GetComponent<Image>().sprite = Sprite.Create(screenShot, new Rect(0f, 0f, Screen.width, Screen.height), Vector2.zero);			
		}*/


		

		
		//yield return new WaitForSeconds(0.5f);
		
		//PhotoPreview.SetActive(true);
		

		
		//yield return StartCoroutine();
		
		yield return new WaitForEndOfFrame(); 
		
		//CheckIfFotoSaved();
	}
	
	public void SavePhoto() {
        //  DefaultTrackableEventHandler.screenshot_check = false;
        panelinfo.GetComponent<Panelinfo>().make_it_false();
        StartCoroutine(SavePhotoCor());
	}

   

	public IEnumerator SavePhotoCor() {
		yield return new WaitForSeconds(0.5f);
		StartCoroutine(savePhotoInGallery.SavePhoto(photoData));
		PhotoPreview.SetActive(false);
		CheckIfFotoSaved();
		ButtonsShare.OpenCloseObjectAnimation();
	}
	
	public void SaveAndSharePhoto() {
		LoadingShare.SetActive(true);
        panelinfo.GetComponent<Panelinfo>().make_it_true();
        StartCoroutine(SaveAndSharePhotoCor());
	}

	public IEnumerator SaveAndSharePhotoCor() {
		yield return new WaitForSeconds(0.5f);
		StartCoroutine(savePhotoInGallery.SavePhoto(photoData));
		CheckIfFotoSaved();
		#if UNITY_ANDROID
		AndroidScreenshotSharer.Instance().ShareImage(TITLE, MESSAGE, screenShot);
		#elif UNITY_IPHONE
			AppleScreenshotSharer.Instance().ShareImage(MESSAGE, screenShot);
		#endif
		PhotoPreview.SetActive(false);
		ButtonsShare.OpenCloseObjectAnimation();
	}
	
	private void DeactivateObjects()
	{
	//	CloudCtrl.CloudRecoEnable(false);
		ObjectState.Clear();
		
		for (int i = 0; i < ObjectToDisableAndAcitvate.Length; i ++)
		{
			ObjectState.Add(ObjectToDisableAndAcitvate[i].activeSelf); 
			ObjectToDisableAndAcitvate[i].SetActive(false);
		}
		
		for (int i = 0; i < DisableImages.Length; i ++)
		{
			DisableImages[i].enabled = false;
		}
		
		for (int i = 0; i < DisableScripts.Length; i ++)
		{
			DisableScripts[i].enabled = false;
		}
		
		for (int i = 0; i < ObjectToActivateAndDisable.Length; i ++)
		{
			ObjectToActivateAndDisable[i].SetActive(true);
		}
	}
	
	private IEnumerator GetScreenPhotoBytes()
	{


		yield return null;
	}
	
	private void ActivateObjects()
	{
		//CloudCtrl.CloudRecoEnable(true);
		for (int i = 0; i < ObjectToDisableAndAcitvate.Length; i ++)
		{
			if (ObjectState[i])
			{
				ObjectToDisableAndAcitvate[i].SetActive(true);
			}
		}
		
		for (int i = 0; i < DisableImages.Length; i ++)
		{
			DisableImages[i].enabled = true;
		}
		
		for (int i = 0; i < ObjectToActivateAndDisable.Length; i ++)
		{
			ObjectToActivateAndDisable[i].SetActive(false);
		}
		
		for (int i = 0; i < DisableScripts.Length; i ++)
		{
			DisableScripts[i].enabled = true;
		}
	}
}

public class SavePhotoInGallery : MonoBehaviour
{
	int numberOfPhotos = 0;
	
	#if UNITY_IPHONE
	
	[DllImport("__Internal")]
	private static extern bool saveToGallery( string path );
	
	#endif
	
	public IEnumerator SavePhoto(byte[] bytes)
	{
		numberOfPhotos = UnityEngine.Random.Range(0, 100) + UnityEngine.Random.Range(100, 1000) + UnityEngine.Random.Range(100, 500);
		
		string screenshotFilename = "Mattei" + numberOfPhotos.ToString() + ".png";
		
		bool photoSaved = false;
		
		#if UNITY_IPHONE
		
		if(Application.platform == RuntimePlatform.IPhonePlayer) 
		{
			string path = Application.persistentDataPath + "/" + screenshotFilename;
			System.IO.File.WriteAllBytes(path, bytes);
			
			TakePictureAndSave.LinkToGallery = path;
			
			while(!photoSaved) 
			{
				photoSaved = saveToGallery(path);
				
				yield return StartCoroutine(Wait(.5f));
			}
			
			//UnityEngine.iOS.Device.SetNoBackupFlag(path);
		}
		
		#elif UNITY_ANDROID	
		
		if(Application.platform == RuntimePlatform.Android) 
		{		
			string androidPath = Path.Combine("Mattei", screenshotFilename);
			string path = Path.Combine(Application.persistentDataPath, androidPath);
			string pathonly = Path.GetDirectoryName(path);
			Directory.CreateDirectory(pathonly);
			
			AndroidJavaClass obj = new AndroidJavaClass("com.ryanwebb.androidscreenshot.MainActivity");
			
			System.IO.File.WriteAllBytes(path, bytes);
			
			TakePictureAndSave.LinkToGallery = path;
			
			while(!photoSaved) 
			{
				photoSaved = obj.CallStatic<bool>("addImageToGallery", path);
				
				yield return StartCoroutine(Wait(.5f));
			}
		}
		
		
		#elif UNITY_WP8
		
		if(Application.platform == RuntimePlatform.WP8Player)
		{
			WP8Screenshot.Main.SaveImage(bytes, fileName);
			
			TakePictureAndSave.LinkToGallery = path;
			
			yield return null;
		}
		
		#endif
	}
	
	private IEnumerator Wait(float delay)
	{
		float pauseTarget = Time.realtimeSinceStartup + delay;
		
		while(Time.realtimeSinceStartup < pauseTarget)
		{
			yield return null;	
		}
	}
}














